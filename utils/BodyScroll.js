class BodyScroll {
  constructor() {
    this.scrollTop = 0;
  }

  // static isMobile() {
  //   console.log(
  //     /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
  //       navigator.userAgent
  //     )
  //   );
  //   if (
  //     /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
  //       navigator.userAgent
  //     )
  //   ) {
  //     return true;
  //   }

  //   return false;
  // }

  static getScrollBarWidth() {
    return window.innerWidth - document.body.clientWidth;
  }

  disable() {
    let isMobile = true;
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      isMobile = true;
    } else {
      isMobile = false;
    }

    this.scrollTop = window.pageYOffset;

    const scrollbarWidth = BodyScroll.getScrollBarWidth();

    /**
     * @see https://stackoverflow.com/questions/69589924/ios-15-minimized-address-bar-issue-with-fixed-position-full-screen-content
     */
    document.documentElement.style = "height: 100vh";

    document.body.setAttribute(
      "style",
      `
		  width: 100%; 
		  overflow: ${scrollbarWidth && !isMobile ? "scroll" : "hidden"};
		`
    );

    // let modal = document.querySelector(".modal");
    // let backdrop = document.querySelector(".backdrop");

    // let isBackdrop = setti(() => {
    //   if (typeof backdrop === "object") {
    //     backdrop.style.marginTop = `${this.scrollTop}px`;
    //     modal.style.marginTop = `${this.scrollTop}px`;

    //     clearInterval(isBackdrop);
    //   }
    // }, 200);

    // setTimeout(() => {
    // document.querySelector(
    //   ".backdrop"
    // ).style.marginTop = `${this.scrollTop}px`;
    // document.querySelector(".modal").style.marginTop = `${this.scrollTop}px`;
    // }, 10);

    // ${this.scrollTop}px
    // document.body.style.position = "fixed !important";
    // document.body.style.marginTop = `${-this.scrollTop}px`;
    // document.body.style.width = "100%";
    // document.body.style.overflowY =
    //   scrollbarWidth && !this.isMobile ? "scroll" : "";
  }

  enable() {
    document.documentElement.style = "";
    document.body.style.position = "";
    document.body.style.marginTop = "";
    document.body.style.width = "";
    document.body.style.overflowY = "";

    window.scrollTo(0, this.scrollTop);
  }
}

export default new BodyScroll();
