import { resolve } from "path";
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";

export default defineNuxtConfig({
  ssr: true,

  runtimeConfig: {
    public: {
      buildMode: "development",
      // metapixel: {
      //   default: { id: "437744472564538", pageView: "**" },
      // },
    },
  },

  modules: [
    "@nuxt/content",
    "@vueuse/nuxt",
    "@pinia/nuxt",
    // "nuxt-meta-pixel",
    [
      "yandex-metrika-module-nuxt3",
      {
        id: "97678721",
        webvisor: true,
        // consoleLog: true,
        // clickmap: true,
        // useCDN: false,
        // trackLinks: true,
        // accurateTrackBounce: true,
      },
    ],
  ],

  components: [{ path: "~/components", global: true }],

  vite: {
    plugins: [
      createSvgIconsPlugin({
        iconDirs: [resolve(__dirname, "./assets/icons")],
      }),
    ],
  },

  app: {
    head: {
      charset: "utf-8",
      viewport: "width=device-width, initial-scale=1, maximum-scale=1",
    },
    // pageTransition: { name: "page", mode: "out-in" },
  },
});
