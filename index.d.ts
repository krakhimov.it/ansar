declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $hasSlot (name?: string): boolean
  }
}

declare module '*.svg' {
  const content : string;
  export default content;
}

export {};
